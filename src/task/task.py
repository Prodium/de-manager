import subprocess

class Task:

    def __init__(self, name, script_path, recurrency):
        self.name = name
        self.script_path = script_path
        self.recurrency = recurrency

    def start(self):
        process = subprocess.Popen(self.script_path)
        stdout = process.communicate()
        print(stdout)
        print(f"Executing {self.name}")