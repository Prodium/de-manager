from fritzconnection.lib.fritzhosts import FritzHosts

fh = FritzHosts(address='192.168.1.1', password='dizzy0769', timeout = 10, use_tls = True)

def get_hosts():
    hosts = fh.get_active_hosts()
    result = []
    for host in hosts:
        result.append(host['name'])
    return result

def is_device_connected(device_name):
    return device_name in get_hosts()
