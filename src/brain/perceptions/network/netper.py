from ...perception import Perception
from .utils import is_device_connected
from functools import partial

# Une perception a besoin de souvenirs
# Comme se souvenir de si j etais deja connecte
# A l appel precedent ou non

was_connected = False

def callback(is_connected):
    if is_connected and not was_connected:
        # Perception doit etre une cell aussi pour faire appel a la socket
        print("Welcome back")
    elif not is_connected and was_connected:
        print("Take care of you")

NetPer = Perception("Network", partial(is_device_connected, 'iPhone'))