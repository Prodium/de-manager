import socketio

class ControlCenter(socketio.Namespace):
    def on_connect(self, sid, environ):
        print("New connection to CC")

    def on_disconnect(self, sid):
        print("One connection lost on CC")

    def on_message(self, sid, msg):
        print(f'Message recevied from {sid}: "{msg}"')