import socketio

class Cell:

    def __init__(self):
        self.socket = socketio.Client()
        self.socket.connect('http://127.0.0.1:5100', namespaces=['/cc'])

    def send_message(self, msg):
        self.send('message', data = msg)

    def send(self, event, data, namespace='/cc'):
        self.socket.emit(event, data, namespace)