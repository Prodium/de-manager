# The brain should be able to schedule task (firstly) having skills etc
from task import Task
from .skill import Skill
from .communication import ControlCenter

import socketio
import eventlet
from multiprocessing import Process

class Brain:
    
    def __init__(self):
        self.socket = socketio.Server(cors_allowed_origins='*')
        self.socket.register_namespace(ControlCenter('/cc'))
        self.process = Process(target=self._start_server)
        self.skills = []

    def add_skill(self, skill: Skill):
        self.skills.append(skill)

    def _start_server(self):
        eventlet.wsgi.server(eventlet.listen(('localhost', 5100)), socketio.WSGIApp(self.socket))

    def start(self):
        self.process.start()

    def join(self):
        self.process.join()

    def exit(self):
        self.process.terminate()
