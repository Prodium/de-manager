from .cell import Cell
from .perception import Perception

import time

class PerceptionManager(Cell):

    def __init__(self, default_timer=86400):
        super.__init__(self)
        self.perceptions = []
        self.default_timer = default_timer
        
        self.running = False
        self.socket.on('stop', self.stop)

    def add_perception(self, perception_function, timer=None, callback_function=None):
        try:
            callback_function()
        except:
            raise Exception("A problem occured cannot add perception")
        if not timer: timer = self.default_timer
        self.perceptions.append({"perception": perception_function, "timer": timer, "next_it": time.time() + timer, "callback": callback_function})

    def run(self):
        self.running = True

        while self.running:
            t = time.time()
            for perception in self.perceptions:
                if t >= perception['next_it']:
                    res = perception['perception']()
                    perception['next_it'] = t + perception['timer']

                    if perception['callback']:
                        perception['callback'](res)

    def stop(self):
        self.running = False
