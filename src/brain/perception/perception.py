

class Perception:

    def __init__(self, name, perception_function, timer=None, description=None, callback_function=None):
        self.name = name
        self.function = perception_function
        self.timer = timer
        self.description = description
        self.callback = callback_function
        self.next_it = None
