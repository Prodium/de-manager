#!usr/bin/python3

from fritzconnection.lib.fritzhosts import FritzHosts
import time

fh = FritzHosts(address='192.168.1.1', password='dizzy0769', timeout = 10, use_tls = True)

def get_hosts():
    hosts = fh.get_active_hosts()
    result = []
    for host in hosts:
        result.append(host['name'])
    return result

def is_device_connected(device_name):
    return device_name in get_hosts()

def main():
    is_at_home = False

    while True:
        is_ok = False
        while not is_ok:
            try:
                is_device_co = is_device_connected('iPhone')
                is_ok = True
            except:
                print("Retry")
        if is_at_home and not is_device_co:
            print("Going out")
        elif not is_at_home and is_device_co:
            print("Going in")
        else:
            print()
        
        is_at_home = is_device_co

        time.sleep(60)

if __name__ == "__main__":
    main()